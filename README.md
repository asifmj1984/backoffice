## BatoiLogic Backoffice

**Descripción del proyecto.**
- La parte de adminstracion del proyecto, en el que se se encuentran todas las tablas y se puede hacer CRUD en ellas.
- Se puede cambiar la contraseña del administrador y el email
- Usuario admin con email:  asif@asif.com y contraseña : 12345678 para acceder al menu.

**URL de la página web desplegada.**

[http://backoffice.grup3.ddaw.site](http://backoffice.grup3.ddaw.site)

[http://staging.backoffice.grup3.ddaw.site](http://staging.backoffice.grup3.ddaw.site)

**Imagen preview de la pagina funcionando**
[Pagina de muestra]: https://ibb.co/FqG75rQ

## Despliegue

**Miembros del equipo.**
- Asif Malik Javed
- Juan Gabriel Vicente Valero
- Adolfo Fenollar Pla
- Fabricio Reinozo Asen

### Staging
-IPv4 Public IP: 34.230.56.46   

Puertos: 80, 443

### Producción
-IPv4 Public IP: 3.80.182.5   

Puertos: 80, 443

### Servidor DNS
-IPv4 Public IP: 54.173.253.18   

Puertos: 53, 443


**Servicios y versiones instaladas**

AWS, Nginx, php-fpm, MySQL Server, bind9

**Usuarios creados y privilegios asignados.**

backoffice (tiene permisos en su document root y para reiniciar el php-fpm)

web (tiene permisos en los document root de la app del repartidor y el sitio web)

batoilogic (MYSQL Server): tiene permisos CRUD Password: 1234

batoilogic_admin (MYSQL Server) : tiene todos los permisos (usado para migraciones) Password: 1234

**Subida a producción y staging**

Para el backoffice se ha utilizado deployer

Para app y web se ha utilizado un script sh





