<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'batoilogic-backoffice');

// Project repository
set('repository', 'git@gitlab.com:batoilogic-grupo3/batoilogic-backoffice.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server
add('writable_dirs', []);


// Hosts

host('staging')
    ->hostname('ec2-34-230-56-46.compute-1.amazonaws.com')
    ->user('backoffice')
    ->identityFile('~/.ssh/id_rsa')
    ->set('deploy_path', '/var/www/batoilogic-backoffice/html');

host('production')
    ->hostname('ec2-3-80-182-5.compute-1.amazonaws.com')
    ->user('backoffice')
    ->identityFile('~/.ssh/id_rsa')
    ->set('deploy_path', '/var/www/batoilogic-backoffice/html');

// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');

desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:vendors',
    'deploy:writable',
    'artisan:storage:link',
    'artisan:view:cache',
    'artisan:config:cache',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
]);

# Declaración de la tarea
task('reload:php-fpm', function () {
    run('sudo /etc/init.d/php7.2-fpm restart');
});
# inclusión en el ciclo de despliegue
after('deploy', 'reload:php-fpm');

task('upload:env', function () {
    upload('.env', '{{deploy_path}}/shared/.env');
})->desc('Environment setup');

task('artisan:migrate', function(){
    run('{{bin/php}} {{release_path}}/artisan migrate:refresh --seed --force --database=migrations');
});

task('artisan passport:install', function(){
    run('{{bin/php}} {{release_path}}/artisan passport:install');
});
after('deploy', 'artisan passport:install');

task('artisan sweetalert:publish', function(){
    run('{{bin/php}} {{release_path}}/artisan sweetalert:publish');
});

after('artisan passport:install', 'artisan sweetalert:publish');

