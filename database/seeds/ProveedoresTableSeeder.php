<?php

use App\Proveedor;
use Illuminate\Database\Seeder;

class ProveedoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Proveedor::create(array('nombre' => 'Fabricio',
            'email' => 'fabri@fabri.com',
            'apellidos' => 'Reinozo',
            'nif' => '12345678L',
            'direccion' => 'C/ Rafael Pascual, 8, 1º',
            'poblacion' => 'Ibi',
            'provincia' => 'Alicante',
            'cp' => '03440',
            'telefono' => '620011111'));
    }
}
