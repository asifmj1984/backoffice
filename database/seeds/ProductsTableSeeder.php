<?php

use App\Producto;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Producto::create(array('nombre' => 'Gorro de lana',
            'categoria_id' => 1,
            'precio' => 25,
            'unidades' => 1000,
            'proveedor_id' => 1));
        Producto::create(array('nombre' => 'Raton inalambrico',
            'categoria_id' => 1,
            'precio' => 500,
            'unidades' => 100,
            'proveedor_id' => 1));
        Producto::create(array('nombre' => 'Teclado Gamer',
            'categoria_id' => 2,
            'precio' => 99.9,
            'unidades' => 75,
            'proveedor_id' => 1));
        Producto::create(array('nombre' => 'Portatil Acer',
            'categoria_id' => 2,
            'precio' => 600,
            'unidades' => 256,
            'proveedor_id' => 1));
    }
}
