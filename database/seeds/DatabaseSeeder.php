<?php

use App\Categoria;
use App\Cliente;
use App\LineaPedido;
use App\Pedido;
use App\Producto;
use App\Proveedor;
use App\Repartidor;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        User::unguard();
        $this->call(UsersTableSeeder::class);
        User::reguard();

        Proveedor::unguard();
        $this->call(ProveedoresTableSeeder::class);
        Proveedor::reguard();

        Cliente::unguard();
        $this->call(ClientesTableSeeder::class);
        Cliente::reguard();

        Repartidor::unguard();
        $this->call(RepartidoresTableSeeder::class);
        Repartidor::reguard();

        Categoria::unguard();
        $this->call(CategoriasTableSeeder::class);
        Categoria::reguard();

        Producto::unguard();
        $this->call(ProductsTableSeeder::class);
        Producto::reguard();

        Pedido::unguard();
        $this->call(PedidosTableSeeder::class);
        Pedido::reguard();

        LineaPedido::unguard();
        $this->call(LineasPedidosTableSeeder::class);
        LineaPedido::reguard();


        $this->command->info("Todo se ha importado correctamente");


    }
}
