<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(array('email' => 'asif@asif.com',
                           'password' => bcrypt('12345678'),
                           'tipo' => 0));//Admin
        User::create(array('email' => 'adolfo@adolfo.com',
                           'password' => bcrypt('12345678'),
                           'tipo' => 1));// Cliente
        User::create(array('email' => 'juan@juan.com',
                           'password' => bcrypt('12345678'),
                           'tipo' => 2));// Repartidor
        User::create(array('email' => 'fabri@fabri.com',
                           'password' => bcrypt('12345678'),
                           'tipo' => 1)); // Proveedor
        User::create(array('email' => 'nacho@nacho.com',
            'password' => bcrypt('12345678'),
            'tipo' => 2));// Repartidor
        User::create(array('email' => 'pablo@pablo.com',
            'password' => bcrypt('12345678'),
            'tipo' => 1));// Cliente
    }
}
