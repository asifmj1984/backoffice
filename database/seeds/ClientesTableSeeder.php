<?php

use App\Cliente;
use Illuminate\Database\Seeder;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cliente::create(array('user_id' => 2,
            'nombre' => 'Adolfo',
            'apellidos' => 'Fenollar Pla',
            'dni' => '87654321A',
            'direccion' => 'C/ Malvada',
            'poblacion' => 'Belgida',
            'provincia' => 'Alicante',
            'cp' => '46868',
            'telefono' => '666666666'));

        Cliente::create(array('user_id' => 6,
            'nombre' => 'Pablo',
            'apellidos' => 'Alcina',
            'dni' => '87654321A',
            'direccion' => 'C/ Demonio',
            'poblacion' => 'Alcoy',
            'provincia' => 'Alicante',
            'cp' => '03804',
            'telefono' => '646464646'));

        Cliente::create(array('user_id' => 4,
            'nombre' => 'Fabricio',
            'apellidos' => 'Reinozo',
            'dni' => '87654321A',
            'direccion' => 'C/ Demonio',
            'poblacion' => 'Alcoy',
            'provincia' => 'Alicante',
            'cp' => '03804',
            'telefono' => '646464646'));

    }
}
