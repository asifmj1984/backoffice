<?php

use App\Repartidor;
use Illuminate\Database\Seeder;

class RepartidoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Repartidor::create(array('user_id' => 3,
            'nombre' => 'Juan Valero'));
        Repartidor::create(array('user_id' => 5,
            'nombre' => 'Nacho Abad'));
    }

}
