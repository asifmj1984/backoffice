<?php

use App\LineaPedido;
use Illuminate\Database\Seeder;

class LineasPedidosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LineaPedido::create(array(
            'pedido_id' => 1,
            'producto_id' => 2,
            'cantidad' => 1,
            'total' => 500));
        LineaPedido::create(array(
            'pedido_id' => 1,
            'producto_id' => 1,
            'cantidad' => 20,
            'total' => 500));
        LineaPedido::create(array(
            'pedido_id' => 2,
            'producto_id' => 4,
            'cantidad' => 6,
            'total' => 2400));
    }
}
