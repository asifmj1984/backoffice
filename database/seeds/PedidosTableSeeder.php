<?php

use App\Pedido;
use Illuminate\Database\Seeder;

class PedidosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pedido::create(array(
            'fecha_compra' => '2020-01-17',
            'cliente_id' => 1,
            'total_compra' => 1000,
            'fecha_entregado' => null,
            'estado'=> 'En preparacion',
            'repartidor_id'=>1));
        Pedido::create(array(
            'fecha_compra' => '2020-01-01',
            'cliente_id' => 2,
            'total_compra' => 2400,
            'fecha_entregado' =>  '2020-01-21',
            'estado'=> 'Entregado',
            'repartidor_id'=>1));
        Pedido::create(array(
            'fecha_compra' => '2020-01-17',
            'cliente_id' => 2,
            'total_compra' => 1000,
            'fecha_entregado' => null,
            'estado'=> 'En preparacion',
            'repartidor_id'=>2));
        Pedido::create(array(
            'fecha_compra' => '2020-01-01',
            'cliente_id' => 1,
            'total_compra' => 2400,
            'fecha_entregado' =>  '2020-01-21',
            'estado'=> 'Entregado',
            'repartidor_id'=>2));
    }
}
