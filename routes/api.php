<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'Api\AuthController@login');
    Route::post('signup', 'Api\AuthController@signup');

    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'Api\AuthController@logout');
        Route::get('user', 'Api\AuthController@user');
    });
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth'], function () {

});
Route::resource('clientes', 'Api\ClienteController');
Route::resource('users', 'Api\UserController');
Route::resource('proveedores', 'Api\ProductoController');
Route::resource('repartidores', 'Api\RepartidorController');

Route::get('pedidos/{id}/lineaspedido', 'Api\LineaPedidoController@lineasOfPedido');
Route::get('setenruta/{id}', 'Api\PedidoController@setEnRuta');
Route::get('/repartidores/{id}/pedidos', 'Api\PedidoController@pedidosOfRepartidor');
Route::get('/clientes/{id}/pedidos', 'Api\PedidoController@pedidosOfCliente');

Route::get('/setincidencia/{id}', 'Api\PedidoController@setIncidencia');
Route::post('/changePassword/{id}', 'Api\ClienteController@changePassword');
Route::resource('lineaspedido', 'Api\LineaPedidoController');
Route::resource('pedidos', 'Api\PedidoController');
Route::get('/repartidorforpedido', 'Api\RepartidorController@getRepartidorForPedido');
Route::get('/categoria/{id}/productos', 'Api\CategoriasController@productosOfCategoria');
Route::resource('categorias', 'Api\CategoriasController');
Route::resource('productos', 'Api\ProductoController');
