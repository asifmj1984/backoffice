<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()){
        return redirect('home');
    }
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => 'auth'], function () {
    Route::get('/productos/proveedor/{id}', 'ProductoController@getProductosDeProveedor')->name('productosProveedor');
    Route::get('/productos/categoria/{id}', 'ProductoController@getProductosDecategoria')->name('prodCategoria');
    Route::resource('proveedores', 'ProveedorController');
    Route::resource('productos', 'ProductoController');
    Route::resource('categorias', 'CategoriaController');
    Route::resource('clientes', 'ClienteController');
    Route::resource('repartidores', 'RepartidorController');
    Route::resource('pedidos', 'PedidosController');
    Route::get('categoria/del/{id}', 'CategoriaController@destroy')->name('delCat');
    Route::get('proveedor/del/{id}', 'ProveedorController@destroy')->name('delProveedor');
    Route::get('cliente/del/{id}', 'ClienteController@destroy')->name('delCliente');
    Route::get('/producto/del/{id}', 'ProductoController@destroy')->name('delProd');
    Route::get('repartidor/del/{id}', 'RepartidorController@destroy')->name('delRepartidor');

    Route::get('repartidor/albaranes/{id}', 'RepartidorController@pdf')->name('albaranesRepartidor');


    Route::post('/admin/updateEmail/{id}', 'HomeController@updateEmail')->name('updateEmail');
    Route::get('/admin/updateEmail', 'HomeController@edit')->name('editAdmin');
    Route::post('/admin/updatePassword/{id}', 'HomeController@updatePassword')->name('updatePassword');
    Route::get('/admin/updatePassword', 'HomeController@editPassAdmin')->name('editPassword');

    //Route::get('/pedidos', 'PedidosController@index')->name('pedidos');
    //Route::get('/pedido/{id}', 'PedidosController@show')->name('pedido');
    Route::get('/pedido/cliente/{id}', 'PedidosController@pedidosDeCliente')->name('pedidosCliente');

    Route::get('graficas', 'UserChartController@index')->name('graficas');


});

Route::get('storage/{userid}/{filename}', function ($userid, $filename) {
    return Storage::get('producto'.$userid . '/' . $filename);
});
