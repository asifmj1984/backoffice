<?php

namespace App\Http\Controllers;

use App\Proveedor;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class ProveedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proveedores = Proveedor::all();
        return view('verProveedores', compact('proveedores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addProveedor');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $proveedor = new Proveedor();
        $proveedor->nombre = $request->nombre;
        $proveedor->apellidos = $request->apellidos;
        $proveedor->nif = $request->nif;
        $proveedor->direccion = $request->direccion;
        $proveedor->poblacion = $request->poblacion;
        $proveedor->cp = $request->cp;
        $proveedor->provincia = $request->provincia;
        $proveedor->telefono = $request->telefono;
        $proveedor->email = $request->email;
        $proveedor->save();
        Alert::success('Proveedor añadido')->autoclose(3000);
        return redirect("/proveedores");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proveedor = Proveedor::findOrFail($id);
        return view('editProveedor',compact('proveedor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $proveedor = Proveedor::find($id);
        $proveedor->nombre = $request->input('nombre');
        $proveedor->apellidos = $request->input('apellidos');
        $proveedor->nif = $request->input('nif');
        $proveedor->direccion = $request->input('direccion');
        $proveedor->poblacion = $request->input('poblacion');
        $proveedor->provincia = $request->input('provincia');
        $proveedor->cp = $request->input('cp');
        $proveedor->email = $request->input('email');
        $proveedor->telefono = $request->input('telefono');
        $proveedor->save();
        Alert::success('Proveedor actualizado')->autoclose(3000);
        return redirect("/proveedores");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $proveedor = Proveedor::findOrFail($id);
        $proveedor->delete();
        Alert::success('Proveedor eliminado')->autoclose(3000);
        return redirect("/proveedores");
    }
}
