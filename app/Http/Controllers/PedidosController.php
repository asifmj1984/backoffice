<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\LineaPedido;
use App\Pedido;
use App\Repartidor;
use Illuminate\Http\Request;

class PedidosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pedidos = Pedido::all();
        return view('verPedidos', compact('pedidos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pedido = Pedido::findOrFail($id);
        $cliente = Cliente::findOrFail($pedido->cliente_id);
        $repartidores = Repartidor::all();
        $lineasPedido = LineaPedido::where('pedido_id', '=', $id)->get();
        return view('verPedidoConcreto',['cliente'=>$cliente, 'pedido'=>$pedido, 'lineasPedido' => $lineasPedido, 'repartidores' => $repartidores  ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pedidosDeCliente($id){
        $pedidos = Pedido::where('cliente_id', '=', $id)->get();
        return view('verPedidos', compact('pedidos'));
    }
}
