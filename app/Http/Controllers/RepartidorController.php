<?php

namespace App\Http\Controllers;

use App\LineaPedido;
use App\Pedido;
use App\Repartidor;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class RepartidorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $repartidores = Repartidor::all();
        return view('verRepartidores', compact('repartidores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addRepartidor');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (User::where('email', $request->email)->exists()) {
            Alert::warning('Ese email ya existe')->autoclose(3000);
            return view('addRepartidor');
        }else {
            $user = new User();
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->tipo = 2;
            $user->save();
            $repartidor = new Repartidor();
            $repartidor->nombre = $request->nombre;
            $repartidor->user_id = $user->id;
            $repartidor->save();
            Alert::success('Repartidor añadido')->autoclose(3000);
            return redirect("/repartidores");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $repartidor = Repartidor::findOrFail($id);
        $user = User::find($repartidor->user_id);
        return view('editRepartidor',['repartidor'=>$repartidor, 'user'=>$user]);
        //return view('editRepartidor',compact('repartidor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $repartidor = Repartidor::find($id);
        $user = User::find($repartidor->user_id);
        $repartidor->nombre = $request->input('nombre');
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();
        $repartidor->save();
        Alert::success('Repartidor actualizado')->autoclose(3000);
        return redirect("/repartidores");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $repartidor = Repartidor::find($id);
        $repartidor->delete();
        Alert::success('Repartidor eliminado')->autoclose(3000);
        return redirect("/repartidores");
    }

    public function pdf($id)
    {
        $pedidos = Pedido::where('repartidor_id', '=', $id)->with('cliente', 'lineaspedido.producto')->get();

        $pdf = PDF::loadView('pdf-repartidores', compact('pedidos'));

        return $pdf->download('albaranes-repartidor-'.$id.'.pdf');
        return redirect("/repartidores");
    }
}
