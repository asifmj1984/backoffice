<?php

namespace App\Http\Controllers;

use App\Charts\UserChart;
use App\LineaPedido;
use App\Producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserChartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $borderColors = [
            "rgba(255, 99, 132, 1.0)",
            "rgba(22,160,133, 1.0)",
            "rgba(255, 205, 86, 1.0)",
            "rgba(51,105,232, 1.0)",
            "rgba(244,67,54, 1.0)",
            "rgba(34,198,246, 1.0)",
            "rgba(153, 102, 255, 1.0)",
            "rgba(255, 159, 64, 1.0)",
            "rgba(233,30,99, 1.0)",
            "rgba(205,220,57, 1.0)"
        ];
        $fillColors = [
            "rgba(255, 99, 132, 0.2)",
            "rgba(22,160,133, 0.2)",
            "rgba(255, 205, 86, 0.2)",
            "rgba(51,105,232, 0.2)",
            "rgba(244,67,54, 0.2)",
            "rgba(34,198,246, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(255, 159, 64, 0.2)",
            "rgba(233,30,99, 0.2)",
            "rgba(205,220,57, 0.2)"

        ];

        $productos=Producto::all('nombre');

        $cantidades=DB::select('select (select count(*) from lineas_pedido where producto_id=productos.id) as cantidad from productos');
        //dd($grouped);
        $result_cant = [];
        $result_prod=[];
        foreach ($cantidades as $cantidad){
            $result_cant[] = $cantidad->cantidad;
        }
        foreach ($productos as $producto){
            $result_prod[] = $producto->nombre;
        }
        /*dd($result_prod);
        dd($productos);*/
        /*$productos2 = DB::table('productos')
            ->join('lineas_pedido', 'id', '=', 'producto_id')
            ->join('orders', 'users.id', '=', 'orders.user_id')
            ->select('productos.*', 'contacts.phone', 'orders.price')
            ->get();*/

        $usersChart = new UserChart;
        $usersChart->minimalist(true);
        $usersChart->labels($result_prod);
        //dd($productos);
        $usersChart->dataset('Ventas', 'bar', $result_cant)
            ->color($borderColors)
            ->backgroundcolor($fillColors);
        return view('charts', [ 'usersChart' => $usersChart ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
