<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repartidor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RepartidorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Repartidor::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'nombre' => 'required',

        ]);

        if ($validator->fails()) {
            return response($validator->errors(), 400);
        }

        try {
            $repartidor = new Repartidor();
            $repartidor->fill($request->toArray());
            $repartidor->save();
            return response($repartidor, 201);

        } catch (\Exception $e) {
            return response($e->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(Repartidor::find($id), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $repartidor = Repartidor::find($id);
        $repartidor->update($request->all());
        return response($repartidor, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $repartidor = Repartidor::find($id);
        $repartidor->delete();
        return response(200);
    }

    public function getRepartidorForPedido(){
        $repartidores=Repartidor::all();
        $ids=[];
        foreach ($repartidores as $repartidor){
            $ids[]=$repartidor->id;
        }
        return $ids[array_rand($ids)];
    }
}
