<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Pedido;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PedidoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Pedido::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fecha_compra' => 'required',
            'cliente_id' => 'required',
            'total_compra' => 'required',
            'repartidor_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response($validator->errors(), 400);
        }

        try {
            $pedido = new Pedido();
            $pedido->fill($request->toArray());
            $pedido->save();
            return response($pedido, 201);

        } catch (\Exception $e) {
            return response($e->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(Pedido::find($id), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pedido = Pedido::find($id);
        $pedido->update($request->all());
        return response($pedido, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pedido = Pedido::find($id);
        $pedido->delete();
        return response(200);
    }

    public function setEnRuta($id)
    {
        Pedido::where('repartidor_id', $id)->where('estado','!=','Entregado')
            ->update(['estado' => "En ruta"]);
        return response(200);
    }

    public function pedidosOfRepartidor($id)
    {
        return response(Pedido::where('repartidor_id', $id)
            ->where(function ($query) {
                $query->whereNull('fecha_entregado')->
                orwhereDate('fecha_entregado', Carbon::today());
            })->with('cliente')->get()
            , 200);

    }

    public function setIncidencia(Request $request, $id)
    {

        return response(Pedido::where('repartidor_id', $id)
            ->where(function ($query) {
                $query->whereNull('fecha_entregado')->
                orwhereDate('fecha_entregado', Carbon::today());
            })->with('cliente')->get()
            , 200);

    }

    public function pedidosOfCliente($id)
    {
        return response(Pedido::where('cliente_id', $id)->with('lineaspedido.producto')->get(), 200);

    }
}
