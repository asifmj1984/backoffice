<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\LineaPedido;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LineaPedidoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(LineaPedido::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pedido_id' => 'required',
            'producto_id' => 'required',
            'cantidad' => 'required',
            'total' => 'required',

        ]);

        if ($validator->fails()) {
            return response($validator->errors(), 400);
        }

        try {
            $LineaPedido = new LineaPedido();
            $LineaPedido->fill($request->toArray());
            $LineaPedido->save();
            return response($LineaPedido, 201);

        } catch (\Exception $e) {
            return response($e->getMessage(), 500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(LineaPedido::find($id), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $LineaPedido = LineaPedido::find($id);
        $LineaPedido->update($request->all());
        return response($LineaPedido, 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $LineaPedido = LineaPedido::find($id);
        $LineaPedido->delete();
        return response( 200);

    }

    public function lineasOfPedido($id)
    {
        return response(LineaPedido::where('pedido_id', $id)->get(),200);
    }
}
