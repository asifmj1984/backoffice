<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        return view('home');
    }

    public static function edit()
    {
        $user = User::find(auth()->id());
        return view('editEmailAdmin', ['user'=>$user]);
    }

    public static function editPassAdmin()
    {
        $user = User::find(auth()->id());
        return view('editPassAdmin', ['user'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function updateEmail(Request $request)
    {
        $user = User::find(auth()->id());
        $user->email = $request->input('email');
        $user->save();
        Alert::success('Email actualizado')->autoclose(3000);
        return redirect("/home");

    }

    public static function updatePassword(Request $request)
    {
        $user = User::find(auth()->id());
        //dd(bcrypt(($request->input('passwordOriginal'))) ."    ".$user->password);
        if(Hash::check($request->input('passwordOriginal'), $user->password)){
            if($request->input('passwordNuevo') === $request->input('passwordNuevo2')){
                $user->password = Hash::make($request->input('passwordNuevo'));
                $user->save();
                Alert::success('Contraseña actualizada')->autoclose(3000);
                return redirect("/home");
            }else{
                Alert::warning('Los campos de la ocontraseña no coinciden')->autoclose(3000);
                return redirect("/admin/updatePassword");
            }

        }else{
            Alert::warning('La contraseña no coincide')->autoclose(3000);
            return redirect("/admin/updatePassword");
        }

    }

}
