<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\User;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = Cliente::all();
        return view('verClientes', compact('clientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addCliente');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (User::where('email', $request->email)->exists()) {
            Alert::warning('Ese email ya existe')->autoclose(3000);
            return view('addCliente');
        }else{
            $user = new User();
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->tipo = 1;
            $user->save();

            $cliente = new Cliente();
            $cliente->nombre = $request->nombre;
            $cliente->apellidos = $request->apellidos;
            $cliente->dni = $request->dni;
            $cliente->direccion = $request->direccion;
            $cliente->poblacion = $request->poblacion;
            $cliente->cp = $request->cp;
            $cliente->provincia = $request->provincia;
            $cliente->telefono = $request->telefono;
            $cliente->user_id = $user->id;
            $cliente->save();
            Alert::success('Cliente añadido')->autoclose(3000);
            return redirect("/clientes");
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = Cliente::findOrFail($id);
        $user = User::find($cliente->user_id);
        return view('editCliente',['cliente'=>$cliente, 'user'=>$user]);
       // return view('editCliente',compact('cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $cliente = Cliente::find($id);
        $user = User::find($cliente->user_id);
        $cliente->nombre = $request->input('nombre');
        $cliente->apellidos = $request->input('apellidos');
        $cliente->dni = $request->input('dni');
        $cliente->direccion = $request->input('direccion');
        $cliente->poblacion = $request->input('poblacion');
        $cliente->provincia = $request->input('provincia');
        $cliente->cp = $request->input('cp');
        $cliente->telefono = $request->input('telefono');
        $cliente->save();
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();
        Alert::success('Cliente actualizado')->autoclose(3000);
        return redirect("/clientes");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {

        $cliente = Cliente::findOrFail($id);
        $cliente->delete();
        Alert::success('Cliente eliminado')->autoclose(3000);
        return redirect("/clientes");

    }
}
