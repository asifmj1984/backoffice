<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Producto;
use App\Proveedor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos=Producto::all();
        return view("/verProductos", ['productos'=>$productos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias=Categoria::all();
        $proveedores=Proveedor::all();
        return view("/addProducts", ['categorias'=>$categorias, 'proveedores'=>$proveedores]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $producto = new Producto();
        $producto->nombre = $request->nombre;
        $producto->precio = $request->precio;
        $producto->unidades = $request->unidades;
        $producto->categoria_id = $request->categoria_id;
        $producto->proveedor_id = $request->proveedor_id;

        if ($request->hasFile('imagen')) {
            $validator = Validator::make($request->all(), [
              'imagen' => 'required|image|mimes:jpeg,png,jpg|max:2048'
            ]);

            if ($validator->fails()) {
                //falta mensaje error
            }

            $producto->imagen_url=$request->imagen->getClientOriginalName();
            $producto->save();
            Storage::putFileAs('producto'.$producto->id, $request->imagen, $request->imagen->getClientOriginalName());
            Alert::success('Producto añadido')->autoclose(3000);
        }
        else{
            $producto->save();
            Alert::success('Producto añadido')->autoclose(3000);
        }



        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prod = Producto::findOrFail($id);
        $categorias=Categoria::all();
        $proveedores=Proveedor::all();
        return view('editProduct',['prod'=>$prod,  'categorias'=>$categorias, 'proveedores'=>$proveedores]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $producto = Producto::findOrFail($id);
        $producto->nombre = $request->input('nombre');
        $producto->unidades = $request->input('unidades');
        $producto->precio = $request->input('precio');
        $producto->categoria_id = $request->categoria_id;
        $producto->proveedor_id = $request->proveedor_id;
        $producto->save();
        Alert::success('Producto actualizado')->autoclose(3000);
        return redirect("/verProductos");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $producto = Producto::findOrFail($id);
        $producto->delete();
        Alert::success('Producto eliminado')->autoclose(3000);
        return redirect("/verProductos");
    }

    public function getProductosDeProveedor($id){
        $productos = Producto::where('proveedor_id', '=', $id)->get();
        return view("/verProductos", ['productos'=>$productos]);
    }

    public function getProductosDecategoria($id){
        $productos = Producto::where('categoria_id', '=', $id)->get();
        return view("/verProductos", ['productos'=>$productos]);
    }
}
