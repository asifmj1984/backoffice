<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineaPedido extends Model
{
    protected $table = 'lineas_pedido';
    public $timestamps = false;
    protected $fillable=['pedido_id', 'producto_id', 'cantidad', 'total'];

    public function producto()
    {
        return $this->belongsTo('App\Producto', 'producto_id');
    }
    public function pedido()
    {
        return $this->belongsTo('App\Pedido');
    }


}
