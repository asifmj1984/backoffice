<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    public $timestamps = false;

    public function proveedor()
    {
        return $this->hasMany('App\Proveedor', 'proveedor_id');
    }
}
