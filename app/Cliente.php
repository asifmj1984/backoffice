<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    public $timestamps = false;
    protected $fillable=['user_id','nombre','apellidos', 'dni' , 'direccion', 'poblacion', 'provincia', 'cp', 'telefono'];

    public function pedidos()
    {
        return $this->hasMany('App\Pedido', 'cliente_id');
    }

}
