<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    public $timestamps = false;
    protected $fillable=['estado', 'fecha_entregado','incidencia', 'fecha_compra', 'cliente_id', 'total_compra', 'repartidor_id'];

    public function cliente()
    {
        return $this->belongsTo('App\Cliente');
    }

    public function lineaspedido()
    {
        return $this->hasMany('App\LineaPedido');
    }




}
