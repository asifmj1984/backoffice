@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>AÑADIR PROVEEDOR</h1>
        <hr>
        <form action={{route('proveedores.store')}} method="POST">
            @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">NOMBRE</label>
                <input type="text" required class="form-control" id="nombre" name="nombre" aria-describedby="emailHelp"
                       placeholder="NOMBRE">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">APELLIDOS</label>
                <input type="text" required class="form-control" id="apellidos" name="apellidos" aria-describedby="emailHelp"
                       placeholder="APELLIDOS">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">NIF</label>
                <input type="text" maxlength="9" minlength="9"  required class="form-control" id="nif" name="nif" aria-describedby="emailHelp"
                       placeholder="NIF">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">DIRECCION</label>
                <input type="text" required class="form-control" id="direccion" name="direccion" aria-describedby="emailHelp"
                       placeholder="DIRECCION">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">POBLACION</label>
                <input type="text" required class="form-control" id="poblacion" name="poblacion" aria-describedby="emailHelp"
                       placeholder="POBLACION">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">PROVINCIA</label>
                <input type="text" required class="form-control" id="provincia" name="provincia" aria-describedby="emailHelp"
                       placeholder="PROVINCIA">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">CP</label>
                <input type="text" maxlength="5" minlength="5"  required class="form-control" id="cp" name="cp" aria-describedby="emailHelp"
                       placeholder="CP">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">TELEFONO</label>
                <input type="text" maxlength="9" minlength="9"  required class="form-control" id="telefono" name="telefono" aria-describedby="emailHelp"
                       placeholder="TELEFONO">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">EMAIL</label>
                <input type="email" required class="form-control" id="email" name="email" aria-describedby="emailHelp"
                       placeholder="EMAIL">
            </div>
            <button type="submit" class="btn btn-primary mb-2">Añadir Proveedor</button>
        </form>
        <hr>
        <form action={{route('home')}} method="GET">
            <button type="submit" class="btn btn-secondary mb-2">MENU</button>
        </form>
    </div>
@endsection

