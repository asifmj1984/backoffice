<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    @foreach ($pedidos as $pedido)
        @csrf

        <h2>Albarán {{ $pedido->id }}</h2>

        <table class="table table-bordered">
            <tbody>
            <tr>
                <th>Cliente</th>
                <td>{{ $pedido->cliente->nombre . " " . $pedido->cliente->apellidos }}</td>
            </tr>
            <tr>
                <th>Direccion</th>
                <td>{{ $pedido->cliente->direccion . " - " . $pedido->cliente->poblacion . ", "
                        . $pedido->cliente->provincia . " "
                        . $pedido->cliente->cp }}</td>
            </tr>
            <tr>
                <th>Fecha de compra</th>
                <td>{{ date("d/m/Y", strtotime(($pedido->fecha_compra))) }}</td>
            </tr>
            <tr>
                <th>Fecha de entrega</th>
                <td>{{ today()->format('d/m/Y') }}</td>
            </tr>

        </table>

        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Cantidad</th>
                <th>Producto</th>
                <th>Precio</th>
                <th>Importe</th>
            </tr>
            </thead>
            <tbody>
            {{$max = 17}}
            @if(sizeof($pedido->lineaspedido)>15)
                {{ $max+=25 }}
            @endif
            @for ($i = 0; $i<=$max; $i++)
                @if($i < sizeof($pedido->lineaspedido))
                    <tr>
                        <td>{{ $pedido->lineaspedido[$i]->cantidad }}</td>
                        <td>{{ $pedido->lineaspedido[$i]->producto->nombre }}</td>
                        <td>{{ $pedido->lineaspedido[$i]->producto->precio }} €</td>
                        <td>{{ $pedido->lineaspedido[$i]->total }} €</td>
                    </tr>
                @else
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                @endif

            @endfor
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Total: {{ $pedido->total_compra }} €</td>
            </tr>
            </tbody>
        </table>
            <div style="page-break-after: auto"></div>
    @endforeach

</div>

</body>
</html>
