@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>DETALLE PEDIDO {{$pedido->id}}</h1>
        <hr>
        <form action={{route('pedidos.update', $pedido->id)}} method="POST">
            @method('PUT')
            @csrf
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Fecha Compra</label>
                <div class="col-sm-10">
                    <input type="text" disabled class="form-control" name="fecha_compra" value='{{date("d-m-Y", strtotime(($pedido->fecha_compra))) }}'>
                </div>
            </div>
            @if($pedido->fecha_entregado === null)
                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Fecha Entregado</label>
                    <div class="col-sm-10">
                        <input type="date" disabled class="form-control" name="fecha_entegado" >
                    </div>
                </div>
            @else
                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Fecha Entregado</label>
                    <div class="col-sm-10">
                        <input type="text" disabled class="form-control" name="fecha_entegado" value='{{date("d-m-Y", strtotime(($pedido->fecha_entregado))) }}'>
                    </div>
                </div>
            @endif

            <div class="form-group">
                <label for="exampleInputEmail1">Estado</label>
                <input type="text" disabled required class="form-control" id="estado" name="estado" aria-describedby="emailHelp"
                       value='{{$pedido->estado}}'>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Incidencia</label>
                <input type="text" disabled required class="form-control" id="incidencia" name="incidencia" aria-describedby="emailHelp"
                       value='{{$pedido->incidencia}}'>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Total</label>
                <input type="text" disabled required class="form-control" id="password" name="password" aria-describedby="emailHelp"
                       value='{{$pedido->total_compra}} €' >
            </div>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">PRODUCTO</th>
                    <th scope="col">PRECIO</th>
                    <th scope="col">UNIDADES</th>
                    <th scope="col">TOTAL</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($lineasPedido as $lineaPedido)
                    <tr>
                        <th scope="row">{{$lineaPedido->producto_id}}</th>
                        <td>{{\App\Producto::find($lineaPedido->producto_id)->nombre }}</td>
                        <td>{{\App\Producto::find($lineaPedido->producto_id)->precio }} €</td>
                        <td>{{$lineaPedido->cantidad}} ud</td>
                        <td>{{$lineaPedido->total}} €</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="form-group ">
                <label for="repartidor_id">Repartidor</label>
                <select id="repartidor_id" disabled name="repartidor_id" class="form-control">
                    @foreach ($repartidores as $repartidor)
                        @if ($repartidor->id === $pedido->repartidor_id)
                            <option value= {{$repartidor->id}}  selected>{{$repartidor->nombre}}</option>
                        @else
                            <option value= {{$repartidor->id}} >{{$repartidor->nombre}} </option>
                        @endif
                    @endforeach
                </select>
            </div>
            <h2>Detalles del cliente</h2>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Nombre </label>
                <div class="col-sm-10">
                    <input type="text" disabled class="form-control" name="dni" value='{{$cliente->nombre}}'>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail"  class="col-sm-2 col-form-label">Apellidos</label>
                <div class="col-sm-10">
                    <input type="text"  disabled class="form-control" name="direccion" value='{{$cliente->apellidos}}'>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">DNI </label>
                <div class="col-sm-10">
                    <input type="text" disabled class="form-control" name="dni" value='{{$cliente->dni}}'>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail"  class="col-sm-2 col-form-label">Direccion</label>
                <div class="col-sm-10">
                    <input type="text" disabled class="form-control" name="direccion" value='{{$cliente->direccion}}'>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail"  class="col-sm-2 col-form-label">Poblacion</label>
                <div class="col-sm-10">
                    <input type="text" disabled class="form-control" name="poblacion" value='{{$cliente->poblacion}}'>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail"  class="col-sm-2 col-form-label">Provincia</label>
                <div class="col-sm-10">
                    <input type="text" disabled class="form-control" name="provincia" value='{{$cliente->provincia}}'>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail"  class="col-sm-2 col-form-label">CP</label>
                <div class="col-sm-10">
                    <input type="text" disabled class="form-control" name="cp" value='{{$cliente->cp}}'>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail"  class="col-sm-2 col-form-label">Telefono</label>
                <div class="col-sm-10">
                    <input type="text" disabled class="form-control" name="telefono" value='{{$cliente->telefono}}'>
                </div>
            </div>

        </form>
        <hr>
        <form action={{route('home')}} method="GET">
            <button type="submit" class="btn btn-secondary mb-2">MENU</button>
        </form>
    </div>
@endsection
