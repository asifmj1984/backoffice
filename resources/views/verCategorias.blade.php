@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>VER CATEGORIAS</h1>
        <hr>
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">NOMBRE</th>
                <th scope="col" class="text-center">ACCIONES</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($categorias as $categoria)
                <tr>
                    <th scope="row">{{$categoria->id}}</th>
                    <td>{{$categoria->nombre}}</td>
                    <td align="center">
                        <a title="Ver productos" href={{  route('prodCategoria', $categoria->id)}}>
                            <i class="iconify" data-icon="mdi:file-eye-outline" data-inline="false"></i>
                        </a>
                        <a title="Editar categoria" href={{  route('categorias.edit', $categoria->id)}}>
                            <i class="iconify" data-icon="mdi:square-edit-outline" data-inline="false"></i>
                        </a>
                        <a title="Eliminar categoria" onclick="return confirm('¿Quieres eliminar la categoria?')" href={{  route('delCat', $categoria->id)}}>
                            <i class="iconify" data-icon="mdi:trash-can-outline" data-inline="false"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <form action={{route('categorias.create')}} method="GET">
            <button type="submit" class="btn btn-primary mb-2">Añadir Categoria</button>
        </form>
        <hr>
        <form action={{route('home')}} method="GET">
            <button type="submit" class="btn btn-secondary mb-2">MENU</button>
        </form>
    </div>
@endsection
