@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>AÑADIR CATEGORIA</h1>
        <hr>
        <form action={{route('categorias.store')}} method="POST">
            @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">Nombre Categoria </label>
                <input type="text" required class="form-control" id="nombre" name="nombre" aria-describedby="emailHelp"
                       placeholder="Nombre Categoria">
            </div>
            <button type="submit" class="btn btn-primary mb-2">Añadir Categoria</button>
        </form>
        <hr>
        <form action={{route('home')}} method="GET">
            <button type="submit" class="btn btn-secondary mb-2">MENU</button>
        </form>
    </div>
@endsection
