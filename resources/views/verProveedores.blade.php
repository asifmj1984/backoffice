@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>VER PROVEEDORES</h1>
        <hr>
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">NOMBRE</th>
                <th scope="col">APELLIDOS</th>
                <th scope="col">DNI</th>
                <th scope="col">DIRECCION</th>
                <th scope="col">POBLACION</th>
                <th scope="col">CP</th>
                <th scope="col">PROVINCIA</th>
                <th scope="col">TELEFONO</th>
                <th scope="col">EMAIL</th>
                <th scope="col" class="text-center">ACCIONES</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($proveedores as $proveedor)
                <tr>
                    <th scope="row">{{$proveedor->id}}</th>
                    <td>{{$proveedor->nombre}}</td>
                    <td>{{$proveedor->apellidos}}</td>
                    <td>{{$proveedor->dni}}</td>
                    <td>{{$proveedor->direccion}}</td>
                    <td>{{$proveedor->poblacion}}</td>
                    <td>{{$proveedor->cp}}</td>
                    <td>{{$proveedor->provincia}}</td>
                    <td>{{$proveedor->telefono}}</td>
                    <td>{{$proveedor->email}}</td>
                    <td align="center">
                        <a title="Ver productos" href={{  route('productosProveedor', $proveedor->id)}}>
                            <i class="iconify" data-icon="mdi:file-eye-outline" data-inline="false"></i>
                        </a>
                        <a title="Editar proveedor" href={{  route('proveedores.edit', $proveedor->id)}}>
                            <i class="iconify" data-icon="mdi:square-edit-outline" data-inline="false"></i>
                        </a>
                        <a title="Eliminar proveedor" onclick="return confirm('¿Quieres eliminar el proveedor?')" href={{  route('delProveedor', $proveedor->id)}}>
                            <i class="iconify" data-icon="mdi:trash-can-outline" data-inline="false"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <form action={{route('proveedores.create')}} method="GET">
            <button type="submit" class="btn btn-primary mb-2">Añadir Proveedor</button>
        </form>
        <hr>
        <form action={{route('home')}} method="GET">
            <button type="submit" class="btn btn-secondary mb-2">MENU</button>
        </form>
    </div>
@endsection
