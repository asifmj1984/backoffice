@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">MENU</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <a href={{ route('clientes.index')}}>Ver Clientes</a>
                        <br>
                        <a href={{ route('clientes.create')}}>Añadir Cliente</a>
                        <br>
                        <hr>
                        <a href={{ route('proveedores.index')}}>Ver Proveedores</a>
                        <br>
                        <a href={{ route('proveedores.create')}}>Añadir Proveedor</a>
                        <br>
                        <hr>
                        <a href={{ route('productos.index')}}>Ver Productos</a>
                        <br>
                        <a href={{ route('productos.create')}}>Añadir Producto</a>
                        <br>
                        <hr>
                        <a href={{ route('categorias.index')}}>Ver Categorias</a>
                        <br>
                        <a href={{ route('categorias.create')}}>Añadir Categoria</a>
                        <br>
                        <hr>
                        <a href={{ route('repartidores.index')}}>Ver Repartidores</a>
                        <br>
                        <a href={{ route('repartidores.create')}}>Añadir Repartidor</a>
                        <br>
                        <hr>
                        <a href={{ route('pedidos.index')}}>Ver Pedidos</a>
                            <br>
                            <a href={{ route('graficas')}}>Estadisticas</a>
                    </div>
                </div>
                <br>
                <div class="card-header">MANTENIMIENTO ADMIN</div>

                <div class="card-body">
                    <br>
                    <a href={{ route('editAdmin')}}>Cambiar Email</a>
                    <br>
                    <a href={{ route('editPassword')}}>Cambiar Contraseña</a>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
