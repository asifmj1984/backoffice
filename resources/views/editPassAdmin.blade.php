@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>EDITAR CONTRASEÑA ADMIN {{$user->id}}</h1>
        <hr>
        <form action={{route('updatePassword', $user->id)}} method="POST">
            @csrf

            <div class="form-group">
                <label for="exampleInputEmail1">CONTRASEÑA ANTIGUA</label>
                <input type="password" required class="form-control" id="passwordOriginal" name="passwordOriginal"
                       >
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">CONTRASEÑA NUEVA</label>
                <input type="password" required class="form-control" id="passwordNuevo" name="passwordNuevo"
                      >
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">REPETIR CONTRASEÑA NUEVA</label>
                <input type="password" required class="form-control" id="passwordNuevo2" name="passwordNuevo2"
                >
            </div>

            <button type="submit" class="btn btn-primary mb-2">Editar Admin</button>
        </form>
        <hr>
        <form action={{route('home')}} method="GET">
            <button type="submit" class="btn btn-secondary mb-2">MENU</button>
        </form>
    </div>
@endsection

