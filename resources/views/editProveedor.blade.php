@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>EDITAR PROVEEDOR {{$proveedor->id}}</h1>
        <hr>
        <form action={{route('proveedores.update', $proveedor->id)}} method="POST">
            @csrf
            @method('PUT')
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Nombre</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="nombre" value='{{$proveedor->nombre}}'>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Apellidos</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="apellidos" value='{{$proveedor->apellidos}}'>
                </div>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">EMAIL</label>
                <input type="email" required class="form-control" id="email" name="email" aria-describedby="emailHelp"
                       value='{{$proveedor->email}}'>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">NIF</label>
                <div class="col-sm-10">
                    <input type="text" maxlength="9" class="form-control" name="nif" value='{{$proveedor->nif}}'>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Direccion</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="direccion" value='{{$proveedor->direccion}}'>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Poblacion</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="poblacion" value='{{$proveedor->poblacion}}'>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Provincia</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="provincia" value='{{$proveedor->provincia}}'>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">CP</label>
                <div class="col-sm-10">
                    <input type="text" maxlength="5" class="form-control" name="cp" value='{{$proveedor->cp}}'>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Telefono</label>
                <div class="col-sm-10">
                    <input type="text" maxlength="9" class="form-control" name="telefono" value='{{$proveedor->telefono}}'>
                </div>
            </div>

            <button type="submit" class="btn btn-primary mb-2">Editar Proveedor</button>
        </form>
        <hr>
        <form action={{route('home')}} method="GET">
            <button type="submit" class="btn btn-secondary mb-2">MENU</button>
        </form>
    </div>
@endsection

