@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>VER PEDIDOS</h1>
        <hr>
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">FECHA COMPRA</th>
                <th scope="col">CLIENTE</th>
                <th scope="col">FECHA ENTREGADO</th>
                <th scope="col">ESTADO</th>
                <th scope="col">INCIDENCIA</th>
                <th scope="col">TOTAL COMPRA</th>
                <th scope="col" class="text-center">ACCIONES</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($pedidos as $pedido)
                <tr>
                    <th scope="row">{{$pedido->id}}</th>
                    <td>{{date("d-m-Y", strtotime(($pedido->fecha_compra)))}}</td>
                    <td>{{\App\Cliente::find($pedido->cliente_id)->nombre ." ". \App\Cliente::find($pedido->cliente_id)->apellidos}}</td>
                    @if($pedido->fecha_entregado === null)
                    <td>{{(($pedido->fecha_entregado))}}</td>
                    @else
                        <td>{{date("d-m-Y", strtotime(($pedido->fecha_entregado)))}}</td>
                    @endif
                    <td>{{$pedido->estado}}</td>
                    <td>{{$pedido->incidencia}}</td>
                    <td>{{$pedido->total_compra}} €</td>
                    <td align="center">
                        <a title="Ver pedido" href={{  route('pedidos.show', $pedido->id)}}>
                            <i class="iconify" data-icon="mdi:file-eye-outline" data-inline="false"></i>
                        </a>
                </tr>
            @endforeach
            </tbody>
        </table>
        <hr>
        <form action={{route('home')}} method="GET">
            <button type="submit" class="btn btn-secondary mb-2">MENU</button>
        </form>
    </div>
@endsection
