@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>AÑADIR CLIENTE</h1>
        <hr>
        <form action={{route('clientes.store')}} method="POST">
            @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">NOMBRE</label>
                <input type="text" required class="form-control" id="nombre" name="nombre" aria-describedby="emailHelp"
                       placeholder="NOMBRE">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">EMAIL</label>
                <input type="email" required class="form-control" id="email" name="email" aria-describedby="emailHelp"
                       placeholder="EMAIL">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">CONTRASEÑA</label>
                <input type="password" required class="form-control" id="password" name="password" aria-describedby="emailHelp"
                       placeholder="CONTRASEÑA">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">APELLIDOS</label>
                <input type="text" required class="form-control" id="apellidos" name="apellidos" aria-describedby="emailHelp"
                       placeholder="APELLIDOS">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">DNI</label>
                <input type="text" maxlength="9" minlength="9"  required class="form-control" id="dni" name="dni" aria-describedby="emailHelp"
                       placeholder="DNI">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">DIRECCION</label>
                <input type="text" required class="form-control" id="direccion" name="direccion" aria-describedby="emailHelp"
                       placeholder="DIRECCION">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">POBLACION</label>
                <input type="text" required class="form-control" id="poblacion" name="poblacion" aria-describedby="emailHelp"
                       placeholder="POBLACION">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">PROVINCIA</label>
                <input type="text" required class="form-control" id="provincia" name="provincia" aria-describedby="emailHelp"
                       placeholder="PROVINCIA">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">CP</label>
                <input type="text" maxlength="5" minlength="5"  required class="form-control" id="cp" name="cp" aria-describedby="emailHelp"
                       placeholder="CP">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">TELEFONO</label>
                <input type="text" maxlength="9" minlength="9" required class="form-control" id="telefono" name="telefono" aria-describedby="emailHelp"
                       placeholder="TELEFONO">
            </div>
            <button type="submit" class="btn btn-primary mb-2">Añadir Cliente</button>
        </form>
        <hr>
        <form action={{route('home')}} method="GET">
            <button type="submit" class="btn btn-secondary mb-2">MENU</button>
        </form>
    </div>
@endsection
