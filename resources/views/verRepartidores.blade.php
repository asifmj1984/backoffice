@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>VER REPARTIDORES</h1>
        <hr>
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">NOMBRE</th>
                <th scope="col" class="text-center">ACCIONES</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($repartidores as $repartidor)
                <tr>
                    <th scope="row">{{$repartidor->id}}</th>
                    <td>{{$repartidor->nombre}}</td>
                    <td align="center">
                        <a title="Descargar albaranes reparto" href={{  route('albaranesRepartidor', $repartidor->id)}}>
                            <i class="iconify" data-icon="mdi:file-eye-outline" data-inline="false"></i>
                        </a>
                        <a title="Editar repartidor" href={{  route('repartidores.edit', $repartidor->id)}}>
                            <i class="iconify" data-icon="mdi:square-edit-outline" data-inline="false"></i>
                        </a>
                        <a title="Eliminar repartidor" onclick="return confirm('¿Quieres eliminar el repartidor?')" href={{  route('delRepartidor', $repartidor->id)}}>
                            <i class="iconify" data-icon="mdi:trash-can-outline" data-inline="false"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <form action={{route('repartidores.create')}} method="GET">
            <button type="submit" class="btn btn-primary mb-2">Añadir Repartidor</button>
        </form>
        <hr>
        <form action={{route('home')}} method="GET">
            <button type="submit" class="btn btn-secondary mb-2">MENU</button>
        </form>
    </div>
@endsection
