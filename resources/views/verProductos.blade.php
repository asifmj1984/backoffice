@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>VER PRODUCTOS</h1>
        <hr>
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">NOMBRE</th>
                <th scope="col">PRECIO</th>
                <th scope="col">UNIDADES</th>
                <th scope="col">PROVEEDOR</th>
                <th scope="col">CATEGORIA</th>
                <th scope="col">FOTO</th>
                <th scope="col" class="text-center">ACCIONES</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($productos as $producto)
            <tr>
                <th scope="row">{{$producto->id}}</th>
                <td>{{$producto->nombre}}</td>
                <td>{{$producto->precio}} €</td>
                <td>{{$producto->unidades}} ud</td>
                <td>{{\App\Proveedor::find($producto->proveedor_id)->nombre." ".\App\Proveedor::find($producto->proveedor_id)->apellidos }}</td>
                <td>{{\App\Categoria::find($producto->categoria_id)->nombre }}</td>

                <td><img src="{{ URL::asset('storage/'.$producto->id.'/'.$producto->imagen_url) }}" height="42" width="42"/>
                    </td>
                <td align="center">
                    <a title="Editar producto" href={{  route('productos.edit', $producto->id)}}>
                        <i class="iconify" data-icon="mdi:square-edit-outline" data-inline="false"></i>
                    </a>
                    <a title="Eliminar producto" onclick="return confirm('¿Quieres eliminar el producto?')" href={{  route('delProd', $producto->id)}}>
                        <i class="iconify" data-icon="mdi:trash-can-outline" data-inline="false"></i>
                    </a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
        <form action={{route('productos.create')}} method="GET">
        <button type="submit" class="btn btn-primary mb-2">Añadir Producto</button>
        </form>
        <hr>
        <form action={{route('home')}} method="GET">
            <button type="submit" class="btn btn-secondary mb-2">MENU</button>
        </form>
    </div>
@endsection
