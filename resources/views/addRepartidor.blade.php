@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>AÑADIR REPARTIDOR</h1>
        <hr>
        <form action={{route('repartidores.store')}} method="POST">
            @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">Nombre </label>
                <input type="text" required class="form-control" id="nombre" name="nombre" aria-describedby="emailHelp"
                       placeholder="Nombre">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">EMAIL</label>
                <input type="email" required class="form-control" id="email" name="email" aria-describedby="emailHelp"
                       placeholder="EMAIL">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">CONTRASEÑA</label>
                <input type="password" required class="form-control" id="password" name="password" aria-describedby="emailHelp"
                       placeholder="CONTRASEÑA">
            </div>
            <button type="submit" class="btn btn-primary mb-2">Añadir Repartidor</button>
        </form>
        <hr>
        <form action={{route('home')}} method="GET">
            <button type="submit" class="btn btn-secondary mb-2">MENU</button>
        </form>
    </div>
@endsection
