@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>EDITAR EMAIL ADMIN {{$user->id}}</h1>
        <hr>
        <form action={{route('updateEmail', $user->id)}} method="POST">
            @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">EMAIL</label>
                <input type="email" required class="form-control" id="email" name="email" aria-describedby="emailHelp"
                       value='{{$user->email}}'>
            </div>
            <button type="submit" class="btn btn-primary mb-2">Editar Admin</button>
        </form>
        <hr>
        <form action={{route('home')}} method="GET">
            <button type="submit" class="btn btn-secondary mb-2">MENU</button>
        </form>
    </div>
@endsection

