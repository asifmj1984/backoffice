@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>EDITAR PRODUCTO</h1>
        <hr>
        <form action={{route('productos.update', $prod->id)}} method="POST">
            @csrf
            @method('PUT')
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Nombre</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="nombre" value='{{$prod->nombre}}'>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Precio</label>
                <div class="col-sm-10">
                    <input type="number" step="0.01" class="form-control" name="precio" value={{$prod['precio']}}>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Unidades</label>
                <div class="col-sm-10">
                    <input type="number" step="1" class="form-control" name="unidades" value={{$prod['unidades']}}>
                </div>
            </div>
            <div class="form-group ">
                <label for="proveedor_id">Proveedor</label>
                <select id="proveedor_id" name="proveedor_id" class="form-control">
                    @foreach ($proveedores as $proveedor)
                        @if ($proveedor->id === $prod->proveedor_id)
                            <option value= {{$proveedor->id}}  selected>{{$proveedor->nombre}}</option>
                        @else
                        <option value= {{$proveedor->id}} >{{$proveedor->nombre}} </option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group ">
                <label for="categoria_id">Categoria</label>
                <select id="categoria_id" name="categoria_id"  class="form-control">
                    @foreach ($categorias as $categoria)
                        @if($categoria->id === $prod->categoria_id)
                            <option value= {{$categoria->id}}  selected>{{$categoria->nombre}}</option>
                        @else
                        <option value= {{$categoria->id}} >{{$categoria->nombre}} </option>
                        @endif
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-primary mb-2">Editar Producto</button>
        </form>
        <hr>
        <form action={{route('home')}} method="GET">
            <button type="submit" class="btn btn-secondary mb-2">MENU</button>
        </form>
    </div>
@endsection
