@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>EDITAR PRODUCTO {{$categoria->id}}</h1>
        <hr>
        <form action={{route('categorias.update', $categoria->id)}} method="POST">
            @csrf
            @method('PUT')
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Nombre</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="nombre" value='{{$categoria->nombre}}'>
                </div>
            </div>

            <button type="submit" class="btn btn-primary mb-2">Editar Categoria</button>
        </form>
        <hr>
        <form action={{route('home')}} method="GET">
            <button type="submit" class="btn btn-secondary mb-2">MENU</button>
        </form>
    </div>
@endsection
