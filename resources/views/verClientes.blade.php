@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>VER CLIENTES</h1>
        <hr>
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">NOMBRE</th>
                <th scope="col">APELLIDOS</th>
                <th scope="col">DNI</th>
                <th scope="col">DIRECCION</th>
                <th scope="col">POBLACION</th>
                <th scope="col">CP</th>
                <th scope="col">PROVINCIA</th>
                <th scope="col">TELEFONO</th>
                <th scope="col" class="text-center">ACCIONES</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($clientes as $cliente)
                <tr>
                    <th scope="row">{{$cliente->id}}</th>
                    <td>{{$cliente->nombre}}</td>
                    <td>{{$cliente->apellidos}}</td>
                    <td>{{$cliente->dni}}</td>
                    <td>{{$cliente->direccion}}</td>
                    <td>{{$cliente->poblacion}}</td>
                    <td>{{$cliente->cp}}</td>
                    <td>{{$cliente->provincia}}</td>
                    <td>{{$cliente->telefono}}</td>
                    <td align="center">
                        <a title="Ver pedidos cliente" href={{  route('pedidosCliente', $cliente->id)}}}>
                            <i class="iconify" data-icon="mdi:file-eye-outline" data-inline="false"></i>
                        </a>
                        <a title="Editar cliente" href={{  route('clientes.edit', $cliente->id)}}>
                            <i class="iconify" data-icon="mdi:square-edit-outline" data-inline="false"></i>
                        </a>
                        <a title="Eliminar cliente" onclick="return confirm('¿Quieres eliminar el cliente?')" href={{  route('delCliente', $cliente->id)}}}>
                            <i class="iconify" data-icon="mdi:trash-can-outline" data-inline="false"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <form action={{route('clientes.create')}} method="GET">
            <button type="submit" class="btn btn-primary mb-2">Añadir Cliente</button>
        </form>
        <hr>
        <form action={{route('home')}} method="GET">
            <button type="submit" class="btn btn-secondary mb-2">MENU</button>
        </form>
    </div>
@endsection
