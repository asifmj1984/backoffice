@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>EDITAR CLIENTE {{$cliente->id}}</h1>
        <hr>
        <form action={{route('clientes.update', $cliente->id)}} method="POST">
            @csrf
            @method('PUT')
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Nombre</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="nombre" value='{{$cliente->nombre}}'>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Apellidos</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="apellidos" value='{{$cliente->apellidos}}'>
                </div>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">EMAIL</label>
                <input type="email" required class="form-control" id="email" name="email" aria-describedby="emailHelp"
                       value='{{$user->email}}'>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">CONTRASEÑA</label>
                <input type="password" required class="form-control" id="password" name="password" aria-describedby="emailHelp"
                       value='{{$user->password}}'>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">DNI</label>
                <div class="col-sm-10">
                    <input type="text" maxlength="9" minlength="9"  class="form-control" name="dni" value='{{$cliente->dni}}'>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Direccion</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="direccion" value='{{$cliente->direccion}}'>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Poblacion</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="poblacion" value='{{$cliente->poblacion}}'>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Provincia</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="provincia" value='{{$cliente->provincia}}'>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">CP</label>
                <div class="col-sm-10">
                    <input type="text" maxlength="5" minlength="5"  class="form-control" name="cp" value='{{$cliente->cp}}'>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Telefono</label>
                <div class="col-sm-10">
                    <input type="text" maxlength="9" minlength="9"  class="form-control" name="telefono" value='{{$cliente->telefono}}'>
                </div>
            </div>

            <button type="submit" class="btn btn-primary mb-2">Editar Cliente</button>
        </form>
        <hr>
        <form action={{route('home')}} method="GET">
            <button type="submit" class="btn btn-secondary mb-2">MENU</button>
        </form>
    </div>
@endsection
