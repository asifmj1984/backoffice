@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>AÑADIR PRODUCTO</h1>
        <hr>
        @if (sizeof($proveedores) != 0 && sizeof($categorias) != 0)
            <form action={{route('productos.store')}} method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="exampleInputEmail1">Nombre Producto </label>
                    <input type="text" required class="form-control" id="nombre" name="nombre"
                           aria-describedby="emailHelp"
                           placeholder="Nombre Prodcuto">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Precio</label>
                    <input type="number" required min="0" step="0.01" class="form-control" id="precio"
                           name="precio" placeholder="Precio">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Unidades</label>
                    <input type="number" required min="0" step="1" class="form-control" id="unidades"
                           name="unidades" placeholder="Unidades">
                </div>
                <div class="form-group ">
                    <label for="proveedor_id">Proveedor</label>
                    <select id="proveedor_id" name="proveedor_id" class="form-control">
                        <option selected>Escoger uno</option>
                        @foreach ($proveedores as $proveedor)
                            <option value= {{$proveedor->id}} >{{$proveedor->nombre." ".$proveedor->apellidos}} </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group ">
                    <label for="categoria_id">Categoria</label>
                    <select id="categoria_id" name="categoria_id" class="form-control">
                        <option selected>Escoger una</option>
                        @foreach ($categorias as $categoria)
                            <option value= {{$categoria->id}} >{{$categoria->nombre}} </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <input type="file" accept="image/*" id="photo_id" name="imagen" class="form-control-file"
                           id="exampleFormControlFile1">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Añadir Producto</button>
            </form>
            <hr>
        @else
            <p>Antes de añadir productos, debes de tener proveedores y categorias en la base de datos.</p>
        @endif
            <form action={{route('home')}} method="GET">
                <button type="submit" class="btn btn-secondary mb-2">MENU</button>
            </form>

    </div>
@endsection
