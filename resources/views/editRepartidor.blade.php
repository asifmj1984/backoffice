@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>EDITAR REPARTIDOR {{$repartidor->id}}</h1>
        <hr>
        <form action={{route('repartidores.update', $repartidor->id)}} method="POST">
            @csrf
            @method('PUT')
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Nombre</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="nombre" value='{{$repartidor->nombre}}'>
                </div>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">EMAIL</label>
                <input type="email" required class="form-control" id="email" name="email" aria-describedby="emailHelp"
                       value='{{$user->email}}'>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">CONTRASEÑA</label>
                <input type="password" required class="form-control" id="password" name="password" aria-describedby="emailHelp"
                       value='{{$user->password}}'>
            </div>

            <button type="submit" class="btn btn-primary mb-2">Editar Repartidor</button>
        </form>
        <hr>
        <form action={{route('home')}} method="GET">
            <button type="submit" class="btn btn-secondary mb-2">MENU</button>
        </form>
    </div>
@endsection
