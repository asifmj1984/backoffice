<?php

return [
    'admin' => 'Gestión Admin',
    'listadoequipos'=>'Listado Equipos',
    'listadocompeticiones'=>'Listado Competiciones',
    'gestionarpartidos'=>'Gestionar Partidos',
    'gestionequipo'=>'Gestión equipo',
    'ultimospartidos'=>'Últimos partidos',
    'proximospartidos'=>'Próximos partidos',
    'nombre'=>'Nombre',
    'cerrarsesion'=>'Cerrar sesión',
    'volver'=>'Volver',
    'gestionpartidos'=>'Gestión Partidos',
    'gestioncompeticiones'=>'Gestión Competiciones',
    'competicion'=>'Competición',
    'fecha'=>'Fecha'
];
